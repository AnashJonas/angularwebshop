package hu.szte.webshop.webshopspring.models;

import java.util.List;

public interface ShopService {
    void addCart(Product cart);
    List<Products> getAllProducts();
}
