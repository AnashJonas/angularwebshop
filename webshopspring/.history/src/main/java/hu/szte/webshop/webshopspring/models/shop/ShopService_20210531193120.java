package hu.szte.webshop.webshopspring.models;

import java.util.List;

public interface ShopService {
    void addCart(ToDo todo);
    List<ToDo> getAllTodos();
    ToDo getToDoById(int id);
    void deleteToDoById(int id);
}
