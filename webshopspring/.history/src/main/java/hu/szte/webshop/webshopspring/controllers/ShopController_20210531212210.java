package hu.szte.webshop.webshopspring.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import hu.szte.webshop.webshopspring.models.Shop;
import hu.szte.webshop.webshopspring.models.ShopService;

@RestController
@RequestMapping
@CrossOrigin(origins = "*")
public class ShopController {

    ShopService shopService;

    @Autowired
    public ShopController(ShopService toDoService) {
        this.toDoService = toDoService;
    }

    @GetMapping("/")
    public String helloWorld() {
        return "Hello World!";
    }

    @PostMapping(path="/shop", consumes = "application/json")
    public String newProduct(@RequestBody Product product) {
        try {
            this.toDoService.addProduct(todo);
            return "Success";
        } catch (Exception e) {
            System.out.println(e);
            return "Error during the create operation";
        }
    }

    @GetMapping("/todos")
    public List<Product> getAllPro() {
        try {
            return this.toDoService.getAllTodos();
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

    @GetMapping("/todo")
    public ToDo getToDoById(@RequestParam int id) {
        try {
            return this.toDoService.getToDoById(id);
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

    @DeleteMapping("/todo")
    public String deleteToDoById(@RequestParam int id) {
        try {
            this.toDoService.deleteToDoById(id);
            return "Delete Successful";
        } catch (Exception e) {
            System.out.println(e);
            return "Error during deletion";
        }
    }
    
}
