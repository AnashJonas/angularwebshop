package hu.szte.webshop.webshopspring.models;

import java.util.List;

public interface ShopService {
    void addCart(Cart cart);
    List<Produ> getAllProducts();
}
