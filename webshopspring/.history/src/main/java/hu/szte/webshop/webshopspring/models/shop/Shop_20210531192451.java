package hu.szte.prf.prfpelda.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "products")
public class Shop {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private String product;
    private int price;

    public Shop() {
    }

    public Shop(String product, int price) {
        this.product = product;
        this.price = price;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "ToDo [description=" + description + ", id=" + id + ", name=" + name + ", priority=" + priority + "]";
    }
    
}
