package hu.szte.webshop.webshopspring.models;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShopServiceImpl implements ShopService {

    ShopRepository shopRepository;

    @Autowired
    public ShopServiceImpl(ShopRepository toDoRepository) {
        this.toDoRepository = toDoRepository;
    }

    @Override
    public void addCart(ToDo todo) {
        this.toDoRepository.save(todo);
        
    }

    @Override
    public List<ToDo> getAllTodos() {
       List<ToDo> list = this.toDoRepository.findAll();
       return list;
    }

    @Override
    public ToDo getToDoById(int id) {
        ToDo todo = this.toDoRepository.findById(id).get();
        return todo;
    }

    @Override
    public void deleteToDoById(int id) {
        this.toDoRepository.deleteById(id);
    }
    
}
