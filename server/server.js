const express = require('express');
const server = express();
const mongoose = require('mongoose');
var router = express.Router();
var cors = require('cors');
const cors = require('cors');
const path = require('path');
var bcrypt = require('bcryptjs');

const cookieParser = require('cookie-parser');
const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const expressSession = require('express-session');

server.use(cookieParser());

const whitelist = ['http://localhost:4200'];
var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed...'))
    }
  },
  credentials: true,
  allowedHeaders: ['Content-Type', 'Authorization', 'Access-Conrol-Allow-Origin', 'Origin', 'Accept']
 };
server.use(cors(corsOptions));


server.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
  next();
});
server.use(cors({origin: true, credentials: true, methods: 'GET,PUT,POST,DELETE,OPTIONS'}));

server.use(expressSession({secret: 'ezegytitok', resave: true}));
server.use(passport.initialize());
server.use(passport.session());

var bodyParser = require('body-parser');
server.use(bodyParser.json()); 
server.use(bodyParser.urlencoded({ extended: false })); 
server.use(express.json()); 
server.use(router);

passport.use('local', new localStrategy( {usernameField: 'email', passwordField: 'pw'}, function(username, password, done) {
  userModel.findOne({email: username}, function(err, user){
    if(err) return done('Hiba a lekérés során!', null);
    if(!user) return done('Nincs ilyen felhasználó!', null);
    bcrypt.compare(password, user.pw, function(error, isMatch) {
      if(error) return done(error, false);
      if(!isMatch) return done('Hibás jelszó!', false);
      return done(null, user);
    })
  })
}));

passport.serializeUser(function(user, done) {
  if(!user) return done('Nincs megadva beléptethető felhasználó!', null);
  return done(null, user);
});

passport.deserializeUser(function(user, done) {
  if(!user) return done('Nincs megadva kiléptethető felhasználó!', null);
  return done(null, user);
});



require('./user.model');
const userModel = mongoose.model('user');

require('./product.model');
const productModel = mongoose.model('product');

mongoose
  .connect(
    'mongodb+srv://adminuser:adminuser1234@angularwebshopcluster.wbsia.mongodb.net/test',
    { useNewUrlParser: true }
  )
  .then(() => {
    console.log("Mongodb connected...");
  })
  .catch(err => {
    console.log("Mongodb error... ", err.stack);
    process.exit(1);
  });


// bejelentkezes
router.route('/login').post((req, res, next) => {
  if(req.body.email, req.body.pw) {
    passport.authenticate('local', function(error, user) {
      if(error) return res.status(500).send({error});
      req.logIn(user, function(error) {
        if(error) return res.status(500).send({error});
        return res.status(200).send('Sikeres bejelentkezés!');
      })
    })(req, res);
  } else {
    return res.status(400).send('Hibás input post.');
  }
});

// kijelentkezes
router.route('/logout').post((req, res, next) => {
  if(req.isAuthenticated()) {
    req.logout();
    return res.status(200).send('Sikeres kijelentkezés!');
  } else {
    return res.status(403).send('Nem is volt bejelentkezve...');
  }
});

// statusz
router.route('/status').post((req, res, next) => {
  if(req.isAuthenticated()) {
    return res.status(200).send(req.session.passport);
  } else {
    return res.status(403).send('Nem is volt bejelentkezve...');
  }
});

// regisztracio
router.route("/registration").get((req, res) => {
  res.status(200);
}).post((req, res) => {
  if(req.body.id && req.body.email) {
    userModel.findOne({id: req.body.id}, (err, data) => {
      if(err) return res.status(500).send('DB hiba');
      if(data) {
        return res.status(400).send('Már létezik ez a felhasználó');
      } else {
        const user = new userModel({
          id: req.body.id, 
          email: req.body.email,
          pw: req.body.pw
        });
        user.save((error) => {
          if(error) return res.status(500).send('A mentés során hiba történt.');
          return res.status(200).send('Sikeres mentés.');
        })
      }
    })
  } else {
    return res.status(400).send('Hibás input post.');
  }
}).put((req, res) => {
  if(req.body.id && req.body.email) {
    userModel.findOne({id: req.body.id}, (err, data) => {
      if(err) return res.status(500).send('DB hiba');
      if(data) {
        user.email = req.body.email;
        user.save((error) => {
          if(error) return res.status(500).send('A mentés során hiba történt.');
          return res.status(200).send('Sikeres mentés.');
        })
      } else {
        return res.status(400).send('Nincs ilyen felhasználó.');
      }
    })
  } else {
    return res.status(400).send('Hibás input.');
  }
});

router.route("/product").get((req, res) => {
  productModel.find({}, (error, product) => {
    if(error) {
      return res.status(500).send(error);
    }
    res.status(200).send(product);
  })
  
}).post((req, res) => {
  if(req.body.product && req.body.price) {
    productModel.findOne({product: req.body.product}, (err, data) => {
      if(err) return res.status(500).send('DB hiba');
      if(data) {
        return res.status(400).send('Már létezik ez a termék');
      } else {
        const user = new productModel({
          product: req.body.product,
          price: req.body.price
        });
        user.save((error) => {
          if(error) return res.status(500).send('A mentés során hiba történt.');
          return res.status(200).send('Sikeres mentés.');
        })
      }
    })
  } else {
    return res.status(400).send('Hibás input post.');
  }
}).put((req, res) => {
  if(req.body.product && req.body.price) {
    productModel.findOne({product: req.body.product}, (err, data) => {
      if(err) return res.status(500).send('DB hiba');
      if(data) {
        data.product = req.body.product;
        data.price = req.body.price;
        data.save((error) => {
          if(error) return res.status(500).send('A mentés során hiba történt.');
          return res.status(200).send('Sikeres mentés.');
        })
      } else {
        return res.status(400).send('Nincs ilyen termék.');
      }
    })
  } else {
    return res.status(400).send('Hibás input put.');
  }
});


server.listen(3000, () => console.log("mongodb-s programom port:3000"));

module.exports = router;