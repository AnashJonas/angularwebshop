const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

var userSchema = new mongoose.Schema({
    id: {type: String, unique: true, required: true, lowercade: true},
    email: {type: String, required: true},
    pw: {type: String, required: true}
}, {collection: 'user'});

userSchema.pre('save', function(next) {
    const user = this;
    if(user.isModified('pw')) {
        user.accessLevel = 'basic';
        bcrypt.genSalt(10, function(err, salt) {
            if(err) {
                console.log('Hiba a salt generálása során!');
                return next(error);
            }
            bcrypt.hash(user.pw, salt, function(error, hash) {
                if(error) {
                    console.log('Hiba a hashelés során!');
                    return next(error);
                }
                user.pw = hash;
                return next();
            })
        })

    } else {
        return next();
    }
});

mongoose.model('user', userSchema);