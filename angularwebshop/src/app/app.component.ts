import { Component } from '@angular/core';
import { ConnectionService } from './util/connection.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angularwebshop';

  constructor(private connectionService: ConnectionService) {}

}
