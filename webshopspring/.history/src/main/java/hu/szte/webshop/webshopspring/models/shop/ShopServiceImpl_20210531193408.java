package hu.szte.webshop.webshopspring.models;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShopServiceImpl implements ShopService {

    ShopRepository shopRepository;

    @Autowired
    public ShopServiceImpl(ShopRepository shopRepository) {
        this.shopRepository = shopRepository;
    }

    @Override
    public void addCart(Product product) {
        this.shopRepository.save(product);
        
    }

    @Override
    public List<Shop> getAllTodos() {
       List<ToDo> list = this.toDoRepository.findAll();
       return list;
    }

    @Override
    public ToDo getToDoById(int id) {
        ToDo todo = this.toDoRepository.findById(id).get();
        return todo;
    }

    @Override
    public void deleteToDoById(int id) {
        this.toDoRepository.deleteById(id);
    }
    
}
