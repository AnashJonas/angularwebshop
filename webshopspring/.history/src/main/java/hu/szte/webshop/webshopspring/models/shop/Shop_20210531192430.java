package hu.szte.prf.prfpelda.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "products")
public class Shop {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private String product;
    private int price;

    public Shop() {
    }

    public Shop(String product, int price) {
        this.name = product;
        this.description = description;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "ToDo [description=" + description + ", id=" + id + ", name=" + name + ", priority=" + priority + "]";
    }
    
}
