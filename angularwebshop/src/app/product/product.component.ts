import { Component, OnInit } from '@angular/core';
import { ProductService} from '../util/product.service';
import { Product } from '../cart/product';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  productList: Product[] = [];
  constructor(private prodService: ProductService, private router: Router) { }

  ngOnInit(): void {
    this.getProduct();
  }

  getProduct(): void {
    this.prodService.getProduct().subscribe((data) => {
      this.productList = [];
      for (const i of data) {
        this.productList.push(i);
      }
    });
  }

  add(item: Product): void {
    if (item.product !== '' && item.price !== null){
      let cartArray = [];
      if (localStorage.getItem('cart') !== null) {
        cartArray = JSON.parse('' + localStorage.getItem('cart'));
      }
      cartArray.push({product: item.product, price: item.price});
      localStorage.setItem('cart', JSON.stringify(cartArray));
    }
  }

  cart(): void {
    this.router.navigate(['cart']);
  }

}
