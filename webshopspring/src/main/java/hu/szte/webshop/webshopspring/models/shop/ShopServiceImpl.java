package hu.szte.webshop.webshopspring.models;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShopServiceImpl implements ShopService {

    ShopRepository shopRepository;

    @Autowired
    public ShopServiceImpl(ShopRepository shopRepository) {
        this.shopRepository = shopRepository;
    }

    @Override
    public void addCart(Product product) {
        this.shopRepository.save(product);
        
    }

    @Override
    public List<Product> getAllProducts() {
       List<Product> list = this.shopRepository.findAll();
       return list;
    }
    
}
