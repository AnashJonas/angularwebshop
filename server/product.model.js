const mongoose = require('mongoose');

var productSchema = new mongoose.Schema({
    product: {type: String, required: true},
    price: {type: Number, required: true}
}, {collection: 'product'});


mongoose.model('product', productSchema);
