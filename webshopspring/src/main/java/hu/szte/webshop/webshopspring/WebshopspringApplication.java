package hu.szte.webshop.webshopspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebshopspringApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebshopspringApplication.class, args);
	}

}
