import { Component, OnInit } from '@angular/core';
import { LoginService } from '../util/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: string;
  password: string;

  constructor(private loginService: LoginService, private router: Router) {
    this.email = '';
    this.password = '';
  }


  login(): void {
    if (this.email !== '' && this.password !== '') {
      this.loginService.login(this.email, this.password).subscribe(msg => {
        console.log(msg);
        localStorage.setItem('user', this.email);
        this.router.navigate(['product']);
      }, error => {
        console.log(error);
      });
    }
  }

  ngOnInit(): void {
    if (localStorage.getItem('user')) {
      localStorage.removeItem('user');
      this.loginService.logout().subscribe(msg => {
        console.log(msg);
      }, error => {
        console.log(error);
      });
    }
  }

}
