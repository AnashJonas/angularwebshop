import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  login = (email: string, password: string) => {
    return this.http.post(environment.serverUrl + 'login', {email: '' + email, pw: '' + password}, {responseType: 'text'});
  }

  logout = () => {
    return this.http.post(environment.serverUrl + 'logout', {}, {withCredentials: true, responseType: 'text'});
  }
}
