package hu.szte.prf.prfpelda.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "products")
public class Shop {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private String product;
    private int price;

    public Shop() {
    }

    public Shop(String product, int price) {
        this.product = product;
        this.price = price;
    }


    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int priority) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "ToDo [description=" + description + ", id=" + id + ", name=" + name + ", priority=" + priority + "]";
    }
    
}
