import { Component, OnInit } from '@angular/core';
import { ProductService} from '../util/product.service';
import { Product } from './product';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  cartArray: Array<Product>;

  constructor(private router: Router) {
    this.cartArray = [];
  }

  ngOnInit(): void {
    if (localStorage.getItem('cart') !== null) {
      this.cartArray = JSON.parse('' + localStorage.getItem('cart'));
    }
  }

  buy(): void {
    this.router.navigate(['cart']);
  }

  back(): void {
    this.router.navigate(['product']);
  }

}
