package hu.szte.webshop.webshopspring.models;

import java.util.List;

public interface ShopService {
    void addToDo(ToDo todo);
    List<ToDo> getAllTodos();
    ToDo getToDoById(int id);
    void deleteToDoById(int id);
}
